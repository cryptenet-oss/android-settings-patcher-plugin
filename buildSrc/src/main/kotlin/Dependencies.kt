/*_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`
 Copyright 2020 Cryptenet Limited

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 _`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`*/

object Dependencies {
    private object Versions {
        const val KOTLIN_POET = "1.6.0"
        const val JUNIT = "4.13"
        const val ASSERT_J = "3.16.1"
    }

    const val KOTLIN_STD = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${CoreVersions.KOTLIN}"
    const val KOTLIN_POET = "com.squareup:kotlinpoet:${Versions.KOTLIN_POET}"
    const val JUNIT = "junit:junit:${Versions.JUNIT}"
    const val ASSERT_J = "org.assertj:assertj-core:${Versions.ASSERT_J}"
}
