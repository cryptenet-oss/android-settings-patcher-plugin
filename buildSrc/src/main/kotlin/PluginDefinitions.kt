@file:Suppress("unused")

import kotlin.reflect.full.memberProperties

/*_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`
 Copyright (c) 2020 Cryptenet Limited.

 Licensed under the Cryptenet MAD License, Version 1.0 (the "License");
 You may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     https://www.cryptenet.com/licenses/MAD-LICENSE-1.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 _`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`*/

object PluginDefinitions {
    const val GROUP_ID = "com.cryptenet.android"
    const val ID = "com.cryptenet.android.settings-patcher"
    const val NAME = "android-settings-patcher-plugin"
    const val DISPLAY_NAME = "Android Settings Patcher Plugin"
    const val DESCRIPTION = "The easiest way to move to new gradle 6 with kotlin dsl"
    const val VERSION = "1.0.0-SNAPSHOT"
    const val JAVA_VERSION = "1.8"
    const val IMPLEMENTATION_CLASS = "com.cryptenet.android.patcher.settings.SettingsPatcherPlugin"
    const val WEBSITE = "https://bitbucket.org/cryptenet-oss/android-settings-patcher-plugin/"
    const val VCS_URL = "https://bitbucket.org/cryptenet-oss/android-settings-patcher-plugin/"
}

object Tags {
    const val TAG_ANDROID = "android"
    const val TAG_ANDROID_SETTINGS = "android-settings"
    const val TAG_ANDROID_SETTINGS_PATCHER = "android-settings-patcher"

    fun getAllTags() = Tags::class.memberProperties
        .filter { it.isConst }
        .map { it.getter.call().toString() }
        .toSet()
}
