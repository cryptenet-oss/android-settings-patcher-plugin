/*_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`
 Copyright 2020 Cryptenet Limited

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 _`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`*/

object BuildPlugins {
    const val KOTLIN_JVM = "org.jetbrains.kotlin.jvm"
    const val KTLINT = "org.jlleitschuh.gradle.ktlint"
    const val JACOCO = "jacoco"
    const val GRADLE_PLUGIN_DEVELOPMENT = "org.gradle.java-gradle-plugin"
    const val MAVEN_PUBLISH = "org.gradle.maven-publish"
    const val GRADLE_PLUGIN_PUBLISH = "com.gradle.plugin-publish"
}
