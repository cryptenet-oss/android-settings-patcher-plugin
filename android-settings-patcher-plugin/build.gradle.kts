plugins {
    with(BuildPlugins) {
        id(KOTLIN_JVM)
        id(KTLINT)
        id(GRADLE_PLUGIN_DEVELOPMENT)
        id(MAVEN_PUBLISH)
        id(GRADLE_PLUGIN_PUBLISH)
        id(JACOCO)
    }
}

dependencies {
    compileOnly(gradleApi())
    with(Dependencies) {
        implementation(KOTLIN_STD)
        implementation(KOTLIN_POET)
        testImplementation(JUNIT)
        testImplementation(ASSERT_J)
    }
    testImplementation(gradleTestKit())
}

val sourcesJar = tasks.register<Jar>("sourcesJar") {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    val localRepoPath = "${rootProject.rootDir}/repo"
    repositories {
        maven(url = uri(localRepoPath))
    }
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
}

gradlePlugin {
    plugins {
        create(PluginDefinitions.NAME) {
            with(PluginDefinitions) {
                id = ID
                displayName = DISPLAY_NAME
                description = DESCRIPTION
                implementationClass = IMPLEMENTATION_CLASS
            }
        }
    }
}

pluginBundle {
    with(PluginDefinitions) {
        website = WEBSITE
        vcsUrl = VCS_URL
    }
    tags = Tags.getAllTags().toList()
}

jacoco {
    toolVersion = CoreVersions.JACOCO
}

tasks.jacocoTestReport {
    reports {
        csv.isEnabled = false
        xml.isEnabled = false
        html.isEnabled = true
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport)
}
