/*_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`
 Copyright (c) 2020 Cryptenet Limited.

 Licensed under the Cryptenet MAD License, Version 1.0 (the "License");
 You may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     https://www.cryptenet.com/licenses/MAD-LICENSE-1.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 _`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`_`*/

plugins {
    with(BuildPlugins) {
        id(KOTLIN_JVM) version CoreVersions.KOTLIN
        id(KTLINT) version CoreVersions.KTLINT
        id(GRADLE_PLUGIN_PUBLISH) version CoreVersions.GRADLE_PLUGIN_PUBLISH
    }
}


allprojects {
    group = "com.cryptenet.android"
    version = "1.0.0-SNAPSHOT"

    repositories {
        mavenCentral()
    }
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = PluginDefinitions.JAVA_VERSION
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = PluginDefinitions.JAVA_VERSION
    }
}
