# Android Settings Patcher Plugin
---
[![Language](https://img.shields.io/badge/language-Kotlin-blue.svg?style=popout&logo=kotlin)](https://www.kotlinlang.org)
[![Platform](https://img.shields.io/badge/platform-Android-green.svg?style=popout&logo=android)](https://www.android.com)
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=popout&logo=apache)](https://bitbucket.org/cryptenet-oss/android-settings-patcher-plugin/src/master/LICENSE)
[![Release Date](https://img.shields.io/badge/release%20date-08.2020-blue.svg?style=popout)](https://bitbucket.org/cryptenet-oss/android-settings-patcher-plugin/src/master/README.md)
[![Version](https://img.shields.io/badge/version-1.0.0-brightgreen.svg?style=popout)](https://bitbucket.org/cryptenet-oss/android-settings-patcher-plugin/downloads/?tab=tags)


## With this plugin
---
1. No buildScript in project level build.gradle.kts
1. Just add this plugin in settings
1. Customize defaults with `pluginResolver` block
1. Autogenerates reference files with applied plugins and versions in buildSrc. Use them in build.gradle.kts files.
1. Run `moduleResolver` task to inject modules in settings and generate files in buildSrc for referenceing in build.gradle.kts files. No more worring about `include()` statements.
 
## How to use
---

```kotlin
// settings.gradle.kts

plugins {
    id("com.cryptenet.android.settings-patcher") version "1.0.0"
}
```

## Customization
---

```kotlin
// settings.gradle.kts

plugins {
    id("com.cryptenet.android.settings-patcher") version "1.0.0"
}

configure<com.cryptenet.android.patcher.settings.extensions.PluginResolverExtension> {
    repositories {
        // "gradlePluginPortal" for gradlePluginPortal()
        // "google" for google()
        // "jcenter" for jcenter()
        // "mavenCentral" for mavenCentral()
        // "mavenLocal" for mavenLocal()
        // for others use url
        repository("repository_url")
    }
    
    plugins {
        // adding or updating plugin version
        plugin(
            "plugin_id",
            "plugin_version",
            true, // `true` if needs legacy plugin, default `false`
            "group_id", // if 3rd option is `true`
            "artifact_id", // if 3rd option is `true`
            "legacy_version" // only if `legacy_version` is different from `plugin_version` and 3rd option is `true`
        )
    }

    // resolution strategy will be resolved automatically if legacy plugin options are provided
}
```

---
##### Copyright (c) 2020 CRYPTENET LIMITED

This software is Licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) (LICENSE)
